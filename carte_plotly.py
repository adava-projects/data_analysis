# -*- coding: utf-8 -*-
"""
Created on Sat Mar 26 23:55:43 2022

@author: Elise
"""

import pandas as pd
import numpy as np

from datetime import datetime
from dateutil.relativedelta import relativedelta
from statsmodels.tsa.seasonal import seasonal_decompose

import geopandas as gpd
from shapely.geometry import Point

import plotly.express as px


path = 'C:/Users/epupier/OneDrive - Groupe Berto/Documents/ELISE/Codes D4G/ADAVA'

#---Import des lieux de comptage
lieux = pd.read_csv(path + '/Data/lieux_comptage.csv',sep=';')
lieux['geometry'] = [Point(xy) for xy in zip(lieux['lon'],lieux['lat'])]
lieux = gpd.GeoDataFrame(lieux)



#---Import des données de comptage & prep time series

data_adava = pd.read_csv(path+'/Data/data_adava_prep.csv',sep=';')

#convertir les dates en format date
data_adava['date'] = pd.to_datetime(data_adava['date'])

#enlever les données de juillet et août 
juil_aout = (pd.DatetimeIndex(data_adava['date']).month == 7)|(pd.DatetimeIndex(data_adava['date']).month == 8)
data_adava = data_adava[~juil_aout]

#enlever les données de mai et juin 2016 où il y a pas mal de points de comptages manquants
year_month = pd.to_datetime(data_adava['date']).dt.to_period('M')
mai_juin_16 = (year_month=='2016-05')| (year_month=='2016-06')
data_adava = data_adava[~mai_juin_16]

data_adava.reset_index(drop=True,inplace=True)

#erreurs sur les dates : mettre toutes les dates avec day=1 et h:m:s.ms = 00:00:00.000
data_adava['date'] = data_adava['date'].apply(lambda t: datetime(t.year, t.month, 1, 0, 0, 0, 0))

#ne garder que les vélos (code à changer si on veut plus tard inclure les trottinettes et autres)
data_adava = data_adava[data_adava['type_velo'].isin(['VAE','Vélos'])]


#-Décomposer la série temporelle totale pour extraire la saisonnalité, utile pour imputer les données manquantes

# série temporelle du nombre de vélo moyen (par lieu) compté par mois
data_by_month_tot_velos = data_adava.groupby(['date','lieu'],as_index=False).sum() #somme sur tous les types de voies et de vélo
data_by_month_mean = data_by_month_tot_velos.groupby(['date'],as_index=False).mean()
ts_by_month_mean = data_by_month_mean.set_index('date')

#décomposition de la série temporelle (period=10 car il n'y a pas de données en juillet-août)
ts_by_month_mean_decompose = seasonal_decompose(ts_by_month_mean, model='additive',period=10,extrapolate_trend=1)

df_lieu_mois = data_adava.groupby(['lieu','date'])['nombre'].sum()
df_lieu_mois = pd.DataFrame(df_lieu_mois).reset_index()

ts_lieux = pd.pivot_table(df_lieu_mois,values='nombre',index='date',columns='lieu')

#imputation des données manquantes
#choix : calculer la moyenne annuelle (+/- 6 mois) sur ce lieu et ajouter la composante saisonnière calculée plus haut (saisonnalité)
ts_by_month_lieu_imp = ts_lieux.copy()

for lieu1 in ts_lieux.columns:
    for date1 in ts_lieux.index:
        if np.isnan(ts_lieux.loc[date1,lieu1]):
            moy_an_lieu = ts_lieux.loc[(date1-relativedelta(months=7)):(date1+relativedelta(months=7)),lieu1].mean()
            sais_mois = ts_by_month_mean_decompose.seasonal.loc[date1]
            ts_by_month_lieu_imp.loc[date1,lieu1] = round(moy_an_lieu + sais_mois,3)#.nombre
            
            
#tendance pour chaque lieu
ts_by_month_lieu_decompose_trend = pd.DataFrame(columns=ts_by_month_lieu_imp.columns)
ts_by_month_lieu_decompose_seasonal = pd.DataFrame(columns=ts_by_month_lieu_imp.columns)

for lieu1 in ts_by_month_lieu_imp.columns:
    ts_by_month_lieu_decompose_trend[lieu1] = seasonal_decompose(ts_by_month_lieu_imp[lieu1], model='additive',period=10,extrapolate_trend=1).trend
    ts_by_month_lieu_decompose_seasonal[lieu1] = seasonal_decompose(ts_by_month_lieu_imp[lieu1], model='additive',period=10,extrapolate_trend=1).seasonal




#---Carte interactive
    
#faire une df avec : date, point, valeur => repivoter
df_trends_velos = ts_by_month_lieu_decompose_trend.reset_index().melt(id_vars = 'date', var_name = 'lieu', value_name = 'nb_velos')
df_trends_velos = df_trends_velos.merge(lieux)
df_trends_velos['date_my'] = df_trends_velos['date'].apply(lambda d: str(d)[0:7])

#changer les noms de variables pour clarifier
df_trends_velos.rename(columns={'nb_velos':'Nb vélos','date_my':'Mois','lat':'Latitude','lon':'Longitude'},inplace=True)

#arrondir le nombre de vélos pour la légende
df_trends_velos['Nb vélos'] = df_trends_velos['Nb vélos'].apply(lambda nv:round(nv))


#-Carte
fig = px.scatter_mapbox(
    df_trends_velos,
    lat="Latitude",
    lon="Longitude",
    size="Nb vélos",
    hover_name='lieu',
    animation_frame="Mois",
    custom_data=['lieu','Mois', 'Nb vélos']
)

fig.update_layout(
    mapbox={"style": "carto-positron", "zoom":12.5}, margin={"l": 0, "r": 0, "t": 0, "b": 0}
)
    
#étiquette personnalisée
fig.update_traces(
    hovertemplate="<br>".join([
        "<b>%{customdata[0]}</b> <br>",
        "%{customdata[1]} : %{customdata[2]} vélos <br><i>(moyenne glissante)</i>"
    ])
)

#pour accélérer l'avancée du temps
fig.layout.updatemenus[0].buttons[0].args[1]["frame"]["duration"] = 200
    
fig.show()